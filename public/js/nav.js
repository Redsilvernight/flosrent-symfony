
function genereSection(section)
{
    var url = "nav/" + section;
    axios.get(url).then(function (response) {
        $('#section-content').empty().html(response.data);
    });
}
