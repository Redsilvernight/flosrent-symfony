<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $hasheur;

    public function __construct(UserPasswordHasherInterface $hasheur)
    {
        $this->hasheur= $hasheur;
    }
    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setEmail('florent-1@orange.fr')
            ->setUsername('redsilvernight')
            ->setPassword($this->hasheur->hashPassword($user,"testadmin"))
            ->setRoles(['ROLE_ADMIN',])
            ->setIsVerified(true)
            ->setFirstName("florent")
            ->setLastName("sabio");

        $manager->persist($user);
        $manager->flush();
    }
}
