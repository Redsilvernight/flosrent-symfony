<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NavController extends AbstractController
{
    /**
     * @Route("/nav/account", name="nav_account")
     */
    public function account(): Response
    {
        return $this->render('sections/comptes.html.twig');
    }

    /**
     * @Route("/nav/explorations", name="nav_explorations")
     */
    public function explorations(): Response
    {
        return $this->render('sections/explorations.html.twig');
    }

    /**
     * @Route("/nav/stats", name="nav_stats")
     */
    public function stats(): Response
    {
        return $this->render('sections/statistiques.html.twig');
    }
}
